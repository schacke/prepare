# Preparation of the vipercom development environment

## Abstract
The vipercom development environment allows to build a software for a communication station. This software runs on a specific hardware consisting of a [96board](https://www.96boards.org) ([dragonboard 410c](https://www.96boards.org/product/dragonboard410c)) and a special hardware board that has been developped excactly for this purpose.  
The preparation installs the required software and then initiates the build process. If no error occurs, the build process ends up with a firmware for the communication station that will be installed on the 96board.

## Requirements
* a clean installation of Ubuntu (version 18.04 recommended) with a minimum of 8 GB RAM and 256 GB disk space
* an authorized gitlab user that is member of the vipercom developers group

## Private directory
A private directory is not mandatory. You can use it optionally to personalize the preparation of your development environment. A private directory is typically located on a network or cloud drive so that it can be easily used and reused. It can contain some or all of the following sub folders: 
* ssh-key: folder containing the SSH key files (private and public) for your gitlab account. The prepare script will automatically copy your ssh key to the ubuntu system so that the step "Install a SSH key for your gitlab account" can be skipped.
* init: folder containing the file INIT that will be sourced by the preparation script to personalize some defines, for example your git user name and email or the IP address of your vipercom development device; the INIT file can personalize the following defines
    - GITUSER: your git username
    - GITEMAIL: your git commit email address
    - GITMIRROR: folder for local mirror repositories
    - VIPERCOM_IP: IP address of the first vipercom device
    - VIPERCOM2_IP: IP address of the second vipercom device
* downloads: folder containing the downloads for the yocto build. If a yocto build has been done before, the downloads can be pushed to this folder (by using the alias push_downloads). So the downloads can be used for a later installation to accelerate the new build.
* sublime-text: folder for the sublime text editor. If the folder contains a sublime text license file, it will be installed automatically while the preparation. The folder is also used to store your sublime text workspace and project files.
* fonts: folder that contains font files to be installed on the ubuntu system

## Install a SSH key for your gitlab account
* if you use a private directory with a ssh folder (see section above) this step can be skipped
* generate a SSH key
> ssh-keygen -t ed25519
* install the public part of the key (content of the file .ssh/id_ed25519.pub) as SSH key in your gitlab account (Settings/SSH keys)

## Starting the preparation
* ensure that git is installed on your system
> sudo apt-get install git
* go to your home folder
> cd 
* clone this repository
> git clone https://gitlab.com/schacke/prepare.git
* go to the prepare folder
> cd prepare
* start the preparation script
> ./prepare vipercom

## Yocto
The preparation script uses the currently supported yocto version for the preparation. If you want to migrate to a newer yocto version, you can start the preparation script with the name of the new yocto version. 
> ./prepare vipercom _name-of-yocto-version_

The migration to a new yocto version requires generally the adaptation of some files. At least the file layer.conf in the meta-vipercom repository has to be updated to indicate the compatibility with the new yocto version.
